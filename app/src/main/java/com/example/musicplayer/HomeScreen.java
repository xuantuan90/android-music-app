package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musicplayer.adapter.PlayListAdapter;
import com.example.musicplayer.adapter.TrackAdapter;
import com.example.musicplayer.model.data.PlayListData;
import com.example.musicplayer.model.data.TrackData;
import com.example.musicplayer.model.result.PlayListResult;
import com.example.musicplayer.model.result.RegisterResult;
import com.example.musicplayer.model.result.TrackResult;
import com.example.musicplayer.services.NetworkHelper;
import com.example.musicplayer.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;

public class HomeScreen extends AppCompatActivity {

    private RecyclerView rvTrack;
    private TextView tvSearch;
    private RecyclerView rvPlayList;
    private TrackAdapter trackAdapter;
    private PlayListAdapter playListAdapter;
    private List<TrackData> trackDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen);
        getSupportActionBar().hide();
        ImageView toProfile = findViewById(R.id.avatar);
        rvTrack = findViewById(R.id.rvTrack);
        rvPlayList = findViewById(R.id.rvPlaylist);
        tvSearch = findViewById(R.id.tvSearch);
        tvSearch.setOnClickListener(view -> {
            startActivity(new Intent(HomeScreen.this, Search.class));
        });
        toProfile.setOnClickListener(v -> startActivity(new Intent(HomeScreen.this, Profile.class)));


        trackAdapter = new TrackAdapter(this, trackDataList, trackData -> {
            int pos = trackDataList.indexOf(trackData);
            Intent i = new Intent(HomeScreen.this, Player.class);
            i.putExtra("PLAYER_URL", trackData.AmThanh);
            i.putExtra("KEY_POS", pos);
            i.putParcelableArrayListExtra("SONG_LIST", (ArrayList<? extends Parcelable>) trackDataList);
            startActivity(i);
        }, playListData -> {

            final Dialog dialog = new Dialog(HomeScreen.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_addplaylist);

            EditText MaPl = (EditText) dialog.findViewById(R.id.MaPl);
            EditText MaBh = (EditText) dialog.findViewById(R.id.MaBh);

            Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
            btnAdd.setOnClickListener(v -> {
                callApiAddPlayList(MaPl.getText().toString(), MaBh.getText().toString());
                dialog.dismiss();
            });

            dialog.show();

        });

        playListAdapter = new PlayListAdapter(this, new ArrayList<>(), playListData -> {
            Intent i = new Intent(HomeScreen.this, PlayListScreen.class);
            i.putExtra("MaPL", playListData.MaPL);
            startActivity(i);
        });

        rvPlayList.setAdapter(playListAdapter);
        rvTrack.setAdapter(trackAdapter);

        callApiGetBH();
        callApiGetPlayListData(PreferenceUtils.getMaUser());

    }


    private void callApiGetBH() {
        Call<TrackResult> call = NetworkHelper.getRequest(this).getDataBH();
        call.enqueue(new retrofit2.Callback<TrackResult>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(@NonNull retrofit2.Call<TrackResult> call, @NonNull retrofit2.Response<TrackResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        trackDataList.clear();
                        trackDataList.addAll(response.body().data.stream().limit(3).collect(Collectors.toList()));
                        trackAdapter.setLstData(trackDataList);
                    } else {
                        Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<TrackResult> call, @NonNull Throwable t) {

            }
        });
    }


    private void callApiGetPlayListData(String MaUser) {
        Call<PlayListResult> call = NetworkHelper.getRequest(this).getPlayListData(MaUser);
        call.enqueue(new retrofit2.Callback<PlayListResult>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(@NonNull retrofit2.Call<PlayListResult> call, @NonNull retrofit2.Response<PlayListResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        playListAdapter.setLstData(response.body().data);
                    } else {
                        Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<PlayListResult> call, @NonNull Throwable t) {

            }
        });
    }


    private void callApiAddPlayList(String maPL, String maBH) {
        Call<PlayListResult> call = NetworkHelper.getRequest(this).ThemBHVaoPL(maPL, maBH);
        call.enqueue(new retrofit2.Callback<PlayListResult>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(@NonNull retrofit2.Call<PlayListResult> call, @NonNull retrofit2.Response<PlayListResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        Toast.makeText(HomeScreen.this, "Thêm bài hát vào playlist thành công", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<PlayListResult> call, @NonNull Throwable t) {

            }
        });
    }
}