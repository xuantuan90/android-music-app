package com.example.musicplayer;

import static com.example.musicplayer.R.layout.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        getSupportActionBar().hide();
        Button toSignUp = findViewById(R.id.btn_signup);
        Button toSignIn = findViewById(R.id.btn_signin);
        toSignUp.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, SignUp.class)));
        toSignIn.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, SignIn.class)));
    }
}