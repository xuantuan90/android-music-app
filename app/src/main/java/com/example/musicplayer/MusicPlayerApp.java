package com.example.musicplayer;

import android.app.Application;

import com.example.musicplayer.utils.PreferenceUtils;

public class MusicPlayerApp extends Application {

    private static MusicPlayerApp instance = null;

    public static MusicPlayerApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
        PreferenceUtils.init();
    }

    public static synchronized void setInstance(MusicPlayerApp musicPlayerApp) {
        if (instance == null) instance = musicPlayerApp;
    }
}
