package com.example.musicplayer;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.musicplayer.adapter.SongPlayListAdapter;
import com.example.musicplayer.model.data.TrackData;
import com.example.musicplayer.model.result.PlayListResult;
import com.example.musicplayer.model.result.TrackResult;
import com.example.musicplayer.services.NetworkHelper;
import com.example.musicplayer.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by trinhxuantuan on 28/05/2022.
 */
public class PlayListScreen extends AppCompatActivity {
    private ImageView btnBack;
    private RecyclerView rvSongPlayList;
    private SongPlayListAdapter songPlayListAdapter;
    private List<TrackData> songList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playlist);
        getSupportActionBar().hide();

        btnBack = findViewById(R.id.btn_back);
        rvSongPlayList = findViewById(R.id.rvSongPlaylist);

        btnBack.setOnClickListener(view -> {
            finish();
        });


        if (getIntent().getExtras() != null) {
            String MaPL = (String) getIntent().getExtras().get("MaPL");
            songPlayListAdapter = new SongPlayListAdapter(this, songList, trackData -> {
                int pos = songList.indexOf(trackData);
                Intent i = new Intent(this, Player.class);
                i.putExtra("PLAYER_URL", trackData.AmThanh);
                i.putExtra("KEY_POS", pos);
                i.putParcelableArrayListExtra("SONG_LIST", (ArrayList<? extends Parcelable>) songList);
                startActivity(i);
            });

            rvSongPlayList.setAdapter(songPlayListAdapter);

            callApiGetDetailPlayListData(MaPL);

        }

    }


    private void callApiGetDetailPlayListData(String MaPL) {
        Call<TrackResult> call = NetworkHelper.getRequest(this).getDetailPlayListData(MaPL);
        call.enqueue(new retrofit2.Callback<TrackResult>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(@NonNull retrofit2.Call<TrackResult> call, @NonNull retrofit2.Response<TrackResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        songList.clear();
                        songList.addAll(response.body().data);
                        songPlayListAdapter.setLstData(songList);
                    } else {
                        Toast.makeText(PlayListScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PlayListScreen.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<TrackResult> call, @NonNull Throwable t) {

            }
        });
    }

}
