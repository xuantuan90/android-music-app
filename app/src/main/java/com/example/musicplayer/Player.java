package com.example.musicplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSeekBar;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.musicplayer.model.data.SongData;
import com.example.musicplayer.model.data.TrackData;
import com.example.musicplayer.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Player extends AppCompatActivity {

    private MediaPlayer mediaPlayer = new MediaPlayer();
    private Handler handler = null;
    private Runnable runnable = null;
    private int seekSecond = 0;
    private ImageView btnShuffle, prevBtn, playBtn, btnNext, btnRepeat, pauseBtn, btnBack;
    private String mediaUrl;
    private TextView tvEnd, tvStart;
    private AppCompatSeekBar seekBar;
    private boolean isShuffle = false, isRepeat = false;
    private List<SongData> songList = new ArrayList<>();
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player);
        getSupportActionBar().hide();

        btnShuffle = findViewById(R.id.btn_shuffle);
        btnBack = findViewById(R.id.btn_back);
        prevBtn = findViewById(R.id.prevBtn);
        playBtn = findViewById(R.id.playBtn);
        btnNext = findViewById(R.id.btnNext);
        btnRepeat = findViewById(R.id.btnRepeat);
        pauseBtn = findViewById(R.id.pauseBtn);
        seekBar = findViewById(R.id.seekbarDetailListener);
        tvStart = findViewById(R.id.tvStart);
        tvEnd = findViewById(R.id.tvEnd);

        if (getIntent().getExtras() != null) {
            pos = getIntent().getExtras().getInt("KEY_POS");
            mediaUrl = (String) getIntent().getExtras().get("PLAYER_URL");
            songList = getIntent().getExtras().getParcelableArrayList("SONG_LIST");

        }

        this.runOnUiThread(() -> {
            try {
                mediaPlayer.setDataSource(Player.this, Uri.parse(mediaUrl));
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setOnPreparedListener(mediaPlayer -> {

                    playBtn.setVisibility(View.GONE);
                    pauseBtn.setVisibility(View.VISIBLE);
                    mediaPlayer.seekTo(seekSecond);

                    mediaPlayer.start();
                    startUpdaterAudio();
                });

                mediaPlayer.prepareAsync();

                mediaPlayer.setOnCompletionListener(mediaPlayer -> {
                    if (isRepeat) {
                        mediaPlayer.start();
                        startUpdaterAudio();
                    } else {
                        if (isShuffle) {
                            long seed = System.nanoTime();
                            Collections.shuffle(songList, new Random(seed));
                        } else {
                            playBtn.setVisibility(View.VISIBLE);
                            pauseBtn.setVisibility(View.GONE);
                            seekBar.setProgress(0);
                            seekSecond = 0;
                            tvStart.setText("00:00");
                            stopAudio();
                        }
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        Utils.setMediaDuration(Player.this, mediaUrl, tvEnd);

        btnShuffle.setOnClickListener(view -> {
            isShuffle = !isShuffle;

            if (isShuffle) {
                btnShuffle.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            } else {
                btnShuffle.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            }
        });

        playBtn.setOnClickListener(view -> {
            this.runOnUiThread(() -> {
                try {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(Player.this, Uri.parse(mediaUrl));
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                    mediaPlayer.setOnPreparedListener(mediaPlayer -> {
                        if (mediaPlayer.isPlaying()) {
                            playBtn.setVisibility(View.VISIBLE);
                            pauseBtn.setVisibility(View.GONE);
                            mediaPlayer.pause();
                        } else {
                            playBtn.setVisibility(View.GONE);
                            pauseBtn.setVisibility(View.VISIBLE);
                            if (!mediaPlayer.isPlaying() && seekSecond > 0) {
                                mediaPlayer.seekTo(seekSecond);
                            }

                            mediaPlayer.start();
                            startUpdaterAudio();
                        }
                    });

                    mediaPlayer.prepareAsync();

                    mediaPlayer.setOnCompletionListener(mediaPlayer -> {
                        playBtn.setVisibility(View.VISIBLE);
                        pauseBtn.setVisibility(View.GONE);
                        seekBar.setProgress(0);
                        seekSecond = 0;
                        tvStart.setText("00:00");
                        stopAudio();
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        });

        pauseBtn.setOnClickListener(view -> {
            playBtn.setVisibility(View.VISIBLE);
            pauseBtn.setVisibility(View.GONE);
            mediaPlayer.pause();
            stopAudio();
            seekSecond = mediaPlayer.getCurrentPosition();
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mediaPlayer.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnNext.setOnClickListener(view -> {
            forwardAudio();
        });

        prevBtn.setOnClickListener(view -> {
            rewindAudio();
        });

        btnRepeat.setOnClickListener(view -> {
            isRepeat = !isRepeat;
            if (isRepeat) {
                mediaPlayer.setLooping(true);
                btnRepeat.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            } else {
                mediaPlayer.setLooping(false);
                btnRepeat.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            }
        });

        btnBack.setOnClickListener(view -> {
            stopAudio();
            mediaPlayer.reset();
            finish();
        });

    }

    private void rewindAudio() {
        try {
            mediaPlayer.reset();
            if (pos <= songList.size() - 1) {
                pos = pos - 1;
                Uri uri = Uri.parse(songList.get(pos).link);
                Utils.setMediaDuration(Player.this, songList.get(pos).link, tvEnd);
                mediaPlayer.setDataSource(getApplicationContext(), uri);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setOnPreparedListener(mediaPlayer -> {
                    playBtn.setVisibility(View.GONE);
                    pauseBtn.setVisibility(View.VISIBLE);
                    mediaPlayer.start();
                    startUpdaterAudio();
                });
                mediaPlayer.prepareAsync();

            } else {
                pos = 0;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void forwardAudio() {
        try {
            mediaPlayer.reset();
            if (pos < songList.size() - 1) {
                pos = pos + 1;
                Uri uri = Uri.parse(songList.get(pos).link);
                Utils.setMediaDuration(Player.this, songList.get(pos).link, tvEnd);
                mediaPlayer.setDataSource(getApplicationContext(), uri);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setOnPreparedListener(mediaPlayer -> {
                    playBtn.setVisibility(View.GONE);
                    pauseBtn.setVisibility(View.VISIBLE);
                    mediaPlayer.start();
                    startUpdaterAudio();
                });
                mediaPlayer.prepareAsync();

            } else {
                pos = 0;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void startUpdaterAudio() {
        if (handler == null) {
            handler = new Handler();
        }

        runnable = () -> {
            seekBar.setMax(mediaPlayer.getDuration());
            int startTime = mediaPlayer.getCurrentPosition();
            int seek = (seekSecond + (startTime - seekSecond));
            tvStart.setText(Utils.getAudioFileLength(
                    seekSecond > 0 ? String.valueOf(seek) : String.valueOf(startTime),
                    true));
            seekBar.setProgress(seekSecond > 0 ? seek : startTime);
            handler.postDelayed(runnable, 50);
        };

        handler.postDelayed(runnable, 50);
    }

    private void stopAudio() {
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable, null);
            handler = null;
            runnable = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopAudio();
        mediaPlayer.reset();
    }
}