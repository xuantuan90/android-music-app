package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musicplayer.model.result.LoginResult;
import com.example.musicplayer.services.NetworkHelper;
import com.example.musicplayer.utils.PreferenceUtils;

import retrofit2.Call;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        TextView songlb = findViewById(R.id.songlb);
        TextView artistlb = findViewById(R.id.artistlb);
        TextView albumlb = findViewById(R.id.albumlb);
        songlb.setOnClickListener(v -> startActivity(new Intent(Profile.this, MySong.class)));
        artistlb.setOnClickListener(v -> startActivity(new Intent(Profile.this, MyArtist.class)));
        albumlb.setOnClickListener(v -> startActivity(new Intent(Profile.this, MyAlbum.class)));
        ImageView backToHome = findViewById(R.id.backToHome);

        backToHome.setOnClickListener(v -> finish());

        Button logout = findViewById(R.id.btn_logout);

        logout.setOnClickListener(view -> {
            PreferenceUtils.saveToken("");
            callApiLogout();
        });
    }


    private void callApiLogout() {
        Call<LoginResult> call = NetworkHelper.getRequest(this).logout();
        call.enqueue(new retrofit2.Callback<LoginResult>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<LoginResult> call, @NonNull retrofit2.Response<LoginResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        startActivity(new Intent(Profile.this, SignIn.class));
                    } else {
                        Toast.makeText(Profile.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Profile.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<LoginResult> call, @NonNull Throwable t) {

            }
        });
    }
}