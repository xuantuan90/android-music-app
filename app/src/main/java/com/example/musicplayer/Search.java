package com.example.musicplayer;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.musicplayer.adapter.PlayListAdapter;
import com.example.musicplayer.adapter.SongSearchAdapter;
import com.example.musicplayer.adapter.TrackAdapter;
import com.example.musicplayer.model.data.TrackData;
import com.example.musicplayer.model.result.PlayListResult;
import com.example.musicplayer.model.result.TrackResult;
import com.example.musicplayer.services.NetworkHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;

/**
 * Created by trinhxuantuan on 31/05/2022.
 */
public class Search extends AppCompatActivity {

    private RecyclerView rvSearch;
    private ImageView ivBack;
    private EditText search_value;
    private SongSearchAdapter songSearchAdapter;
    private List<TrackData> trackDataListSearch = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_screen);
        getSupportActionBar().hide();
        rvSearch = findViewById(R.id.rvSearch);
        search_value = findViewById(R.id.search_value);
        ivBack = findViewById(R.id.ivBack);

        ivBack.setOnClickListener(view -> finish());


        songSearchAdapter = new SongSearchAdapter(this, trackDataListSearch, trackData -> {
            int pos = trackDataListSearch.indexOf(trackData);
            Intent i = new Intent(Search.this, Player.class);
            i.putExtra("PLAYER_URL", trackData.AmThanh);
            i.putExtra("KEY_POS", pos);
            i.putParcelableArrayListExtra("SONG_LIST", (ArrayList<? extends Parcelable>) trackDataListSearch);
            startActivity(i);
        });

        rvSearch.setAdapter(songSearchAdapter);

        callApiSearch(search_value.getText().toString());

        search_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<TrackData> lsFilter = trackDataListSearch.stream().filter(s -> s.TenBH.contains(charSequence.toString().toLowerCase())).collect(Collectors.toList());
                songSearchAdapter.setLstData(lsFilter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void callApiSearch(String search) {
        Call<TrackResult> call = NetworkHelper.getRequest(this).searchSong(search);
        call.enqueue(new retrofit2.Callback<TrackResult>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(@NonNull retrofit2.Call<TrackResult> call, @NonNull retrofit2.Response<TrackResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        trackDataListSearch.clear();
                        trackDataListSearch.addAll(response.body().data);
                        songSearchAdapter.setLstData(trackDataListSearch);
                    } else {
                        trackDataListSearch.clear();
                        songSearchAdapter.setLstData(trackDataListSearch);
                    }
                } else {
                    Toast.makeText(Search.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<TrackResult> call, @NonNull Throwable t) {

            }
        });
    }
}
