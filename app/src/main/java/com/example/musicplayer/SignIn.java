package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musicplayer.model.result.LoginResult;
import com.example.musicplayer.services.NetworkHelper;
import com.example.musicplayer.utils.PreferenceUtils;
import com.example.musicplayer.utils.Utils;

import retrofit2.Call;

public class SignIn extends AppCompatActivity {

    private EditText edtUserName, edtPassword;
    private TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        getSupportActionBar().hide();
        TextView toForget = findViewById(R.id.lb_forget);
        edtUserName = findViewById(R.id.edtUserName);
        tvError = findViewById(R.id.tvError);
        edtPassword = findViewById(R.id.edtPassword);
        TextView toSignUp = findViewById(R.id.lb_signup);
        Button login = findViewById(R.id.btn_login);
        toForget.setOnClickListener(v -> startActivity(new Intent(SignIn.this, ForgetPassword.class)));
        toSignUp.setOnClickListener(v -> startActivity(new Intent(SignIn.this, SignUp.class)));
        login.setOnClickListener(v -> {
            if (!isValidSignIn()) {
                tvError.setText("Vui lòng nhập đầy đủ thông tin");
                tvError.setVisibility(View.VISIBLE);
                return;
            }

            tvError.setVisibility(View.GONE);
            callApiLogin();
        });
    }

    private boolean isValidSignIn() {
        if (!Utils.isEmpty(edtUserName.getText().toString())
                && !Utils.isEmpty(edtPassword.getText().toString())) {
            return true;
        }

        return false;
    }

    private void callApiLogin() {
        Call<LoginResult> call = NetworkHelper.getRequest(this).login(edtUserName.getText().toString(), edtPassword.getText().toString());
        call.enqueue(new retrofit2.Callback<LoginResult>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<LoginResult> call, @NonNull retrofit2.Response<LoginResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        PreferenceUtils.saveMaUser(response.body().data.MaUser);
                        startActivity(new Intent(SignIn.this, HomeScreen.class));
                    } else {
                        Toast.makeText(SignIn.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SignIn.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<LoginResult> call, @NonNull Throwable t) {

            }
        });
    }
}