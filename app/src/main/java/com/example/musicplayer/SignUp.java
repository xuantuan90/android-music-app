package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musicplayer.model.result.RegisterResult;
import com.example.musicplayer.services.NetworkHelper;
import com.example.musicplayer.utils.Utils;

import org.w3c.dom.Text;

import retrofit2.Call;

public class SignUp extends AppCompatActivity {

    private EditText edtName, edtUserName, edtPassword, edtRePassword;
    private TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        getSupportActionBar().hide();
        Button btnSignup = findViewById(R.id.btn_signup);
        edtName = findViewById(R.id.edtName);
        tvError = findViewById(R.id.tvError);
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtRePassword);

        btnSignup.setOnClickListener(view -> {
            if (!isValidSignup()) {
                tvError.setText("Vui lòng nhập đầy đủ thông tin");
                tvError.setVisibility(View.VISIBLE);
                return;
            }

            tvError.setVisibility(View.GONE);
            callApiRegister();
        });
    }

    private boolean isValidSignup() {
        if (!Utils.isEmpty(edtName.getText().toString())
                && !Utils.isEmpty(edtUserName.getText().toString())
                && !Utils.isEmpty(edtPassword.getText().toString())
                && !Utils.isEmpty(edtRePassword.getText().toString())) {
            return true;
        }

        return false;
    }

    private void callApiRegister() {
        Call<RegisterResult> call = NetworkHelper.getRequest(this).register(edtName.getText().toString(), edtUserName.getText().toString(), edtPassword.getText().toString(), edtRePassword.getText().toString());
        call.enqueue(new retrofit2.Callback<RegisterResult>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<RegisterResult> call, @NonNull retrofit2.Response<RegisterResult> response) {
                if (response.body() != null) {
                    if (response.body().success == 1) {
                        startActivity(new Intent(SignUp.this, SignIn.class));
                    } else {
                        tvError.setText(response.body().message);
                        Toast.makeText(SignUp.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    tvError.setText(response.body().message);
                    Toast.makeText(SignUp.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<RegisterResult> call, @NonNull Throwable t) {

            }
        });
    }
}