package com.example.musicplayer.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.musicplayer.R;
import com.example.musicplayer.model.data.PlayListData;
import com.example.musicplayer.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.ViewHolder> {

    private List<PlayListData> lstData;
    private Context context;
    private OnClickPlayList onClickPlayList;

    public PlayListAdapter(Context context, List<PlayListData> listData, OnClickPlayList onClickTrack) {
        this.lstData = listData;
        this.context = context;
        this.onClickPlayList = onClickTrack;
    }

    public void setLstData(List<PlayListData> lstData) {
        this.lstData = lstData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_playlist, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.itemView.setOnClickListener(view -> {
            onClickPlayList.clickPlayList(lstData.get(holder.getLayoutPosition()));
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlayListData data = lstData.get(position);
        holder.tvPlayList.setText(data.TenPL);
    }

    @Override
    public int getItemCount() {
        return lstData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvPlayList)
        TextView tvPlayList;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickPlayList {
        void clickPlayList(PlayListData trackData);
    }
}
