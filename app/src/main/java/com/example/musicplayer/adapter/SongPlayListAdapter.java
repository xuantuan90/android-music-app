package com.example.musicplayer.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.musicplayer.R;
import com.example.musicplayer.model.data.TrackData;
import com.example.musicplayer.utils.Utils;

import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SongPlayListAdapter extends RecyclerView.Adapter<SongPlayListAdapter.ViewHolder> {

    private List<TrackData> lstData;
    private Context context;
    private OnClickTrack onClickTrack;

    public SongPlayListAdapter(Context context, List<TrackData> listData, OnClickTrack onClickTrack) {
        this.lstData = listData;
        this.context = context;
        this.onClickTrack = onClickTrack;
    }

    public void setLstData(List<TrackData> lstData) {
        this.lstData = lstData;
        notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_song, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.itemView.setOnClickListener(view -> {
            onClickTrack.clickTrack(lstData.get(holder.getLayoutPosition()));
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TrackData data = lstData.get(position);
        holder.tvSong.setText(data.TenBH);
        holder.tvAuthor.setText(data.TenCS);
        Utils.setMediaDuration((Activity) context, data.AmThanh, holder.tvDuration);
        Glide.with(context).load(data.HinhAnh).into(holder.ivAvatar);

    }

    @Override
    public int getItemCount() {
        return lstData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvSong)
        TextView tvSong;
        @BindView(R.id.ivAvatar)
        ImageView ivAvatar;
        @BindView(R.id.tvAuthor)
        TextView tvAuthor;
        @BindView(R.id.tvDuration)
        TextView tvDuration;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickTrack {
        void clickTrack(TrackData trackData);
    }
}
