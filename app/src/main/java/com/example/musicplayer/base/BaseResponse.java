package com.example.musicplayer.base;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("success")
    public int success;
    @SerializedName("message")
    public String message;
}
