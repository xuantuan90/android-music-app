package com.example.musicplayer.model.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by trinhxuantuan on 02/05/2022.
 */
public class PlayListData implements Parcelable {
    public String MaPL;
    public String TenPL;
    public String MaUser;

    public PlayListData(String maPL, String tenPL, String maUser) {
        MaPL = maPL;
        TenPL = tenPL;
        MaUser = maUser;
    }

    protected PlayListData(Parcel in) {
        MaPL = in.readString();
        TenPL = in.readString();
        MaUser = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MaPL);
        dest.writeString(TenPL);
        dest.writeString(MaUser);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlayListData> CREATOR = new Creator<PlayListData>() {
        @Override
        public PlayListData createFromParcel(Parcel in) {
            return new PlayListData(in);
        }

        @Override
        public PlayListData[] newArray(int size) {
            return new PlayListData[size];
        }
    };
}
