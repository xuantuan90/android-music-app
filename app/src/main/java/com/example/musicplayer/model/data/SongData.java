package com.example.musicplayer.model.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by trinhxuantuan on 02/05/2022.
 */
public class SongData implements Parcelable {
    public int id;
    public String avatar;
    public String song;
    public String author;
    public String duration;
    public String link;

    public SongData(int id, String avatar, String song, String author, String duration, String link) {
        this.id = id;
        this.avatar = avatar;
        this.song = song;
        this.author = author;
        this.duration = duration;
        this.link = link;
    }

    protected SongData(Parcel in) {
        id = in.readInt();
        avatar = in.readString();
        song = in.readString();
        author = in.readString();
        duration = in.readString();
        link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(avatar);
        dest.writeString(song);
        dest.writeString(author);
        dest.writeString(duration);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SongData> CREATOR = new Creator<SongData>() {
        @Override
        public SongData createFromParcel(Parcel in) {
            return new SongData(in);
        }

        @Override
        public SongData[] newArray(int size) {
            return new SongData[size];
        }
    };
}
