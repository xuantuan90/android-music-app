package com.example.musicplayer.model.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by trinhxuantuan on 02/05/2022.
 */
public class TrackData implements Parcelable {
    public String MaBH;
    public String TenBH;
    public String TenCS;
    public String TenAlbum;
    public String HinhAnh;
    public String AmThanh;

    public TrackData(String maBH, String tenBH, String tenCS, String tenAlbum, String hinhAnh, String amThanh) {
        MaBH = maBH;
        TenBH = tenBH;
        TenCS = tenCS;
        TenAlbum = tenAlbum;
        HinhAnh = hinhAnh;
        AmThanh = amThanh;
    }

    protected TrackData(Parcel in) {
        MaBH = in.readString();
        TenBH = in.readString();
        TenCS = in.readString();
        TenAlbum = in.readString();
        HinhAnh = in.readString();
        AmThanh = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MaBH);
        dest.writeString(TenBH);
        dest.writeString(TenCS);
        dest.writeString(TenAlbum);
        dest.writeString(HinhAnh);
        dest.writeString(AmThanh);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TrackData> CREATOR = new Creator<TrackData>() {
        @Override
        public TrackData createFromParcel(Parcel in) {
            return new TrackData(in);
        }

        @Override
        public TrackData[] newArray(int size) {
            return new TrackData[size];
        }
    };
}