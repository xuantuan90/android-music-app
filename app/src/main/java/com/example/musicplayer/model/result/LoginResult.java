package com.example.musicplayer.model.result;

import com.example.musicplayer.base.BaseResponse;
import com.example.musicplayer.model.data.LoginData;

/**
 * Created by Admin on 25/2/2019.
 */

public class LoginResult extends BaseResponse {
    public LoginData data;
}
