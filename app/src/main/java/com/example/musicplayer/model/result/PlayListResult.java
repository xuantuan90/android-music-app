package com.example.musicplayer.model.result;

import com.example.musicplayer.base.BaseResponse;
import com.example.musicplayer.model.data.LoginData;
import com.example.musicplayer.model.data.PlayListData;

import java.util.List;

/**
 * Created by Admin on 25/2/2019.
 */

public class PlayListResult extends BaseResponse {
    public List<PlayListData> data;
}
