package com.example.musicplayer.model.result;

import com.example.musicplayer.base.BaseResponse;
import com.example.musicplayer.model.data.TrackData;

import java.util.List;

/**
 * Created by trinhxuantuan on 23/05/2022.
 */
public class TrackResult extends BaseResponse {
    public List<TrackData> data;
}
