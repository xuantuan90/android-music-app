package com.example.musicplayer.services;

import com.example.musicplayer.model.result.LoginResult;
import com.example.musicplayer.model.result.PlayListResult;
import com.example.musicplayer.model.result.RegisterResult;
import com.example.musicplayer.model.result.TrackResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("Server/login.php")
    Call<LoginResult> login(@Query("TaiKhoan") String email,
                            @Query("MatKhau") String password);

    @GET("Server/DangKy.php")
    Call<RegisterResult> register(@Query("Ten") String Ten,
                                  @Query("TaiKhoan") String TaiKhoan,
                                  @Query("MatKhau") String MatKhau,
                                  @Query("rMatKhau") String rMatKhau);

    @GET("Server/logout.php")
    Call<LoginResult> logout();

    @GET("Server/GetBaiHatData.php")
    Call<TrackResult> getDataBH();

    @GET("Server/ThemBHVaoPL.php")
    Call<PlayListResult> ThemBHVaoPL(@Query("MaPL") String MaPL,
                                     @Query("MaBH") String MaBH);
//
    @GET("Server/GetPlayListData.php")
    Call<PlayListResult> getPlayListData(@Query("MaUser") String MaUser);

    @GET("Server/GetChiTietPlaylistData.php")
    Call<TrackResult> getDetailPlayListData(@Query("MaPL") String MaPl);

    @GET("Server/TimBaiHatData.php")
    Call<TrackResult> searchSong(@Query("search") String search);

}
