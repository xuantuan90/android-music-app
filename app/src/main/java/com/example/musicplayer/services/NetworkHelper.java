package com.example.musicplayer.services;

import android.app.Activity;
import android.content.Intent;

import com.example.musicplayer.BuildConfig;
import com.example.musicplayer.SignIn;
import com.example.musicplayer.network.ConnectivityInterceptor;
import com.example.musicplayer.utils.PreferenceUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkHelper {

    private static final String BASE_URL = "https://18521372.000webhostapp.com/";

    public static APIService getRequest(Activity context) {
        HttpLoggingInterceptor interceptor = getHttpLoggingInterceptor();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.addInterceptor(interceptor);
        httpClient.addInterceptor(new ConnectivityInterceptor(context));
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .build();
//            if (!Utils.isEmpty(PreferenceUtils.getToken())) {
//                Log.e("token", PreferenceUtils.getToken());
//                Request requestPre = original.newBuilder()
//                        .header("Content-Type", "application/json")
//                        .header("Authorization", "Bearer " + PreferenceUtils.getToken())
//                        .method(original.method(), original.body())
//                        .build();
//
//                return chain.proceed(requestPre);
//            } else {
            return chain.proceed(request);
//            }
        });

        httpClient.addInterceptor(chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);

            // todo deal with the issues the way you need to
            if (response.code() == 401) {
                logout(context);
            }
            return response;
        });
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Boolean.class, new BooleanTypeAdapter());
        Gson gson = builder.create();
        OkHttpClient client = httpClient.build();
        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(BASE_URL)
                        .client(client)
                        .build();

        return retrofit.create(APIService.class);
    }

    private static void logout(Activity context) {
        PreferenceUtils.saveToken("");
        Intent intent = new Intent(context, SignIn.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    static class BooleanTypeAdapter implements JsonDeserializer<Boolean> {
        public Boolean deserialize(JsonElement json, Type typeOfT,
                                   JsonDeserializationContext context) throws JsonParseException {
            int code = json.getAsInt();
            return code != 0 && (code == 1 ? true : null);
        }
    }
}


