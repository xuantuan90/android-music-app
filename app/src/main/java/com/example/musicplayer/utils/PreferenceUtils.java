package com.example.musicplayer.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.musicplayer.MusicPlayerApp;

public class PreferenceUtils {

    private static final String FLAG_TOKEN = "token";
    private static final String FLAG_MAUSER = "mauser";

    private static SharedPreferences preferences;

    public static synchronized void init() {
        preferences = PreferenceManager.getDefaultSharedPreferences(MusicPlayerApp.getInstance());
    }


    public static void saveToken(String token) {
        preferences.edit().putString(FLAG_TOKEN, token).apply();
    }

    public static String getToken() {
        return preferences.getString(FLAG_TOKEN, "") != null ? preferences.getString(FLAG_TOKEN, "") : "";
    }

    public static void saveMaUser(String MaUser) {
        if (preferences != null) {
            preferences.edit().putString(FLAG_MAUSER, MaUser).apply();
        }

    }

    public static String getMaUser() {
        if (preferences != null) {
            return preferences.getString(FLAG_MAUSER, "") != null ? preferences.getString(FLAG_MAUSER, "") : "";
        }
        return null;
    }


}
