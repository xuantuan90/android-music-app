package com.example.musicplayer.utils;

import android.app.Activity;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import java.util.HashMap;

public class Utils {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm == null || cm.getActiveNetworkInfo() == null;
    }

    public static boolean isEmpty(CharSequence str) {
        return str == null || TextUtils.getTrimmedLength(str) == 0 || "null".equalsIgnoreCase(str.toString().trim());
    }

    public static void setMediaDuration(Activity activity, String url, TextView tvEnd) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(url, new HashMap());
        long time =
                Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        retriever.release();
        String durations =
                convertMillieToHMmSs(time);
        Log.d("durationns", durations);

        activity.runOnUiThread(() -> {
            tvEnd.setText(durations);
        });
    }

    private static String convertMillieToHMmSs(Long millie) {
        long seconds = millie / 1000;
        long second = seconds % 60;
        long minute = seconds / 60 % 60;
        long hour = seconds / (60 * 60) % 24;
        return hour > 0 ? String.format("%02d:%02d:%02d", hour, minute, second) : String.format("%02d:%02d", minute, second);
    }

    public static String getAudioFileLength(String duration, boolean stringFormat) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            int millSecond = Integer.parseInt(duration);
            if (millSecond < 0)
                return String.valueOf(0); // if some error then we say duration is zero
            if (!stringFormat) return String.valueOf(millSecond);
            int hours, minutes, seconds = millSecond / 1000;
            hours = (seconds / 3600);
            minutes = (seconds / 60) % 60;
            seconds = seconds % 60;
            if (hours > 0 && hours < 10) stringBuilder.append("0").append(hours).append(":");
            else if (hours > 0) stringBuilder.append(hours).append(":");
            if (minutes < 10) stringBuilder.append("0").append(minutes).append(":");
            else stringBuilder.append(minutes).append(":");
            if (seconds < 10) stringBuilder.append("0").append(seconds);
            else stringBuilder.append(seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
